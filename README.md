# Simorgh

Launch x-desktop files.

## Install

Run the following command in the source code directory to build and install:

```sh
meson build && cd build && ninja && sudo ninja install
```

Run the following commands in the source code directory to uninstall:

```sh
cd build && sudo ninja uninstall
sudo update-desktop-database /usr/local/share/applications/
```

## Usage

* From the file manager: open a `.desktop` file with Simorgh, like any other file.
* From the command line: `org.codeberg.som.Simorgh my-app.desktop`
